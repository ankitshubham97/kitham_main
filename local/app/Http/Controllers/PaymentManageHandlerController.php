<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\QueryException;


class PaymentManageHandlerController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }

   public function index() {
   		$input = Input::all();
   		// print_r($input);
   		$id = $input['id'];
   		$upi = $input['upi'];
   		if(Auth::user()->id != $id) {
   			abort(403);
   		}
   		if (isset($upi)) {
   			try{
			    DB::table('users')
		            ->where('id', $id)
		            ->update(['upi' => $upi]);
			} catch(QueryException $e) {
				abort(response($e), 403);
			}
   		}
   		return back()->withInput();
   }
}
