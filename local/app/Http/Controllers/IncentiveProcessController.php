<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Hash;

class IncentiveProcessController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('App\Http\Middleware\AdminMiddleware');
    }

   public function index() {
   		$html_course_payments = DB::table('html_course_tbl')
	            ->join('users', 'users.id', '=', 'html_course_tbl.joinee_id')
	            ->select('html_course_tbl.joinee_id as joinee_id',
	            	'users.name as name',
	            	'users.email as email',
	            	'users.payment_image_name as payment_image_name',
	            	'html_course_tbl.amt_paid as amt_paid')
	            ->get();
	    return view('incentive_process',['html_course_payments'=>$html_course_payments]);
   }
}
