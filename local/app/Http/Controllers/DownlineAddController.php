<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Hash;

class DownlineAddController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }

   public function index() {
      return view('downline_add');
   }
}
