<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Illuminate\Database\QueryException;
use Carbon\Carbon;

class DownlineAddHandlerController extends Controller
{
  public function __construct() {
    $this->middleware('auth');
  }

  public function index() {

	$input = Input::all();
  $name = $input['name'];
  $email = $input['email'];
  $course = $input['course'];
  $password = str_random(8);
  $hashed_random_password = Hash::make($password);

  if (!($course == __('messages.html'))) {
    $errmsg = "Currently, course ".$course." is unavailable.";
    return view('downline_add_failure', ['errmsg'=>$errmsg]);
  }
  request()->validate([
            'image_payment' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',]);
  request()->validate([
            'image_identity_proof' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',]);
  $time=time();
  $ext=request()->image_payment->getClientOriginalExtension();
  $imagePaymentName = $email.'_'.$time.'image_payment'.'.'.$ext;
  $imageIdentityProofName = $email.'_'.$time.'image_identity_proof'.'.'.$ext;
  try{
    $id = DB::table('users')->insertGetId(
          ['name' => $name,
           'email' => $email,
           'password' => $hashed_random_password,
           'payment_image_name' => $imagePaymentName,
           'identity_proof_image_name' => $imageIdentityProofName]);
    DB::table('relations_tbl')->insert(
      ['parent_id' => Auth::user()->id, 'child_id' => $id]);
    $users = DB::table('users')
          ->select('id', 'name', 'email')
          ->where('id', '=', $id)
          ->get();
    if ($course == __('messages.html')) {
      DB::table('html_course_tbl')->insert(
        ['joinee_id' => $id, 'joined_on' => Carbon::now()]);
    }
  } catch(QueryException $e) {
    $errmsg = "Unknown error occured: ".$e->errorInfo[1];
    if ($e->errorInfo[1] == 1062) {
      $errmsg = "Email ".$email." already registered in the system.";
    }
    return view('downline_add_failure', ['errmsg'=>$errmsg]);
  }
  
  request()->image_payment->move(public_path('images/payment'), $imagePaymentName);
  request()->image_payment->move(public_path('images/identity_proof'), $imageIdentityProofName);
  Mail::to($email)->send(new SendMailable($name, $password, $course));
  return view('downline_add_success', ['name'=>$name, 'email'=>$email]);
  }
}
