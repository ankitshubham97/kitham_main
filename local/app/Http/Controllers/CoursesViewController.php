<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

class CoursesViewController extends Controller {
  public function __construct() {
    $this->middleware('auth');
  }

  public function index() {
    $show_html = 0;
    $show_html_css = 0;
    $show_html_css_js = 0;
    $show_html = DB::table('html_course_tbl')
            ->select('*')
            ->where('joinee_id', '=', Auth::user()->id)
            ->count();
    if ($show_html > 1 || $show_html_css > 1 || $show_html_css_js > 1) {
      abort( response('Duplicate joinee ids '.$Auth::user()->id, 403) );
    }
    return view('courses_view',['show_html'=>$show_html, 'show_html_css'=>$show_html_css, 'show_html_css_js'=>$show_html_css_js]);  
  }

  public function html() {
    $show_html = DB::table('html_course_tbl')
            ->select('*')
            ->where('joinee_id', '=', Auth::user()->id)
            ->count();
    if ($show_html == 0) {
      abort( response($Auth::user()->email.' is not registered for HTML course', 403) );
    }
    return view('courses_view_html');
  }
}
