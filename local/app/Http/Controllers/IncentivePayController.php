<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Hash;

class IncentivePayController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('App\Http\Middleware\AdminMiddleware');
    }

   public function index() {
   		$incentives = DB::table('incentives_tbl')
	            ->join('users', 'users.id', '=', 'incentives_tbl.senior_id')
	            ->select('incentives_tbl.senior_id as senior_id',
	            	'users.upi as upi',
	            	'incentives_tbl.joinee_id as joinee_id',
	            	'incentives_tbl.course as course',
	            	'incentives_tbl.incentive_obtained as incentive_obtained',
	            	'incentives_tbl.incentive_transaction_id as incentive_transaction_id',
	            	'incentives_tbl.id as id')
	            ->get();
	    foreach ($incentives as $incentive){
	    	if(!(empty($incentive->incentive_transaction_id)) && empty($incentive->upi)) {
	    		abort( response('Transaction Id '.$incentive->incentive_transaction_id.' is non-empty but upi is empty, where did you transfer the money?', 403) );
	    	}
	    }
	    return view('incentive_pay',['incentives'=>$incentives]);
   }
}
