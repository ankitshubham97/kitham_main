<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\QueryException;


class IncentivePayHandlerController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('App\Http\Middleware\AdminMiddleware');
    }

   public function index() {
   		$input = Input::all();
   		$id = $input['id'];
   		$incentive_transaction_id = $input['incentive_transaction_id'];
   		if (empty($incentive->incentive_transaction_id)) {
   			try{
   				if(empty($incentive_transaction_id)) {
   					abort( response('Transaction Id is empty', 403) );
   				}
			    DB::table('incentives_tbl')
		            ->where('id', $id)
		            ->update(['incentive_transaction_id' => $incentive_transaction_id]);
			} catch(QueryException $e) {
				abort(response($e), 403);
			}
   		}
   		return back()->withInput();
   }
}
