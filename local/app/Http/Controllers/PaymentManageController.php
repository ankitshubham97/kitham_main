<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Hash;

class PaymentManageController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }

   public function index() {
   		$user = DB::table('users')
	            ->select('*')
	            ->where('id', '=', Auth::user()->id)
	            ->get();
	     if (count($user) != 1) {
	     	abort(500);
	     }
	    return view('payment_manage',['user'=>$user]);
   }
}
