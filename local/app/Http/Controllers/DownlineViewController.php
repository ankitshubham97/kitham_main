<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

class DownlineViewController extends Controller {
  public $level2;
  public $level3;
  public function __construct() {
    $this->middleware('auth');
  }

  private function validate_id($id) {
    if (Auth::user()->id == $id) {
      return;
    }
    $level2_ids = DB::table('relations_tbl')->select('child_id as level2_id')->where('parent_id', '=', Auth::user()->id)->get();
    foreach($level2_ids as $level2_id){
      if ($level2_id->level2_id == $id) {
        $this->level2 = DB::table('users')
                          ->select('*')
                          ->where('id', '=', $id)
                          ->get();
        return;
      }
    }
    // $level2_3_ids = DB::table('relations_tbl')
    //                   ->select('child_id')
    //                   ->whereIn('parent_id',function($query){
    //                       $query->select('child_id')
    //                             ->from('relations_tbl')
    //                             ->where('parent_id', '=', Auth::user()
    //                             ->id);
    //                           })
    //                   ->get();
    $level2_3_ids = DB::table('relations_tbl as gen1')
                      ->join('relations_tbl as gen2', 'gen1.child_id', '=', 'gen2.parent_id')
                      ->select('gen1.child_id as level2_id', 'gen2.child_id as level3_id')
                      ->get();
    foreach($level2_3_ids as $level2_3_id){
      if ($level2_3_id->level3_id == $id) {
        // $this->level2->id = $grandchild->level2_id;
        // $this->level3->id = $grandchild->level3_id;
        $this->level2 = DB::table('users')
                          ->select('*')
                          ->where('id', '=', $level2_3_id->level2_id)
                          ->get();
        $this->level3 = DB::table('users')
                          ->select('*')
                          ->where('id', '=', $level2_3_id->level3_id)
                          ->get();
        return;
      }
    }
    abort(403);
  }

  public function index($id) {
    $this->validate_id($id);
    $flat_downlines = DB::table('relations_tbl')
            ->join('users', 'users.id', '=', 'relations_tbl.child_id')
            ->select('parent_id', 'child_id', 'name', 'email')
            ->where('parent_id', '=', $id)
            ->get();
    return view('downline_view',['level2'=>$this->level2, 'level3'=>$this->level3, 'flat_downlines'=>$flat_downlines]);  
  }
}
