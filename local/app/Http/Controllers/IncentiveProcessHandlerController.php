<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\QueryException;


class IncentiveProcessHandlerController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('App\Http\Middleware\AdminMiddleware');
    }

   public function index() {
   		$input = Input::all();
   		$joinee_id = $input['joinee_id'];
   		$amt_paid = $input['amt_paid'];
   		$course = $input['course'];
   		if ($course == __('messages.html')) {
   			try{
			    DB::table('html_course_tbl')
		            ->where('joinee_id', $joinee_id)
		            ->update(['amt_paid' => $amt_paid]);
		        $level1_id = DB::table('relations_tbl')
	                          ->select('parent_id')
	                          ->where('child_id', '=', $joinee_id)
	                          ->get();

				if (!count($level1_id)) {
					return;
				}
				DB::table('incentives_tbl')->insert(
				          ['senior_id' => $level1_id[0]->parent_id,
				           'joinee_id' => $joinee_id,
				           'course' => $course,
				           'incentive_obtained' => ($amt_paid)/2]);

				$level2_id = DB::table('relations_tbl')
	                          ->select('parent_id')
	                          ->where('child_id', '=', $level1_id[0]->parent_id)
	                          ->get();

				if (!count($level2_id)) {
					return;
				}

				DB::table('incentives_tbl')->insert(
				          ['senior_id' => $level2_id[0]->parent_id,
				           'joinee_id' => $joinee_id,
				           'course' => $course,
				           'incentive_obtained' => ($amt_paid)/4]);

				$level3_id = DB::table('relations_tbl')
	                          ->select('parent_id')
	                          ->where('child_id', '=', $level2_id[0]->parent_id)
	                          ->get();

	            if (!count($level3_id)) {
					return;
				}

	            DB::table('incentives_tbl')->insert(
				          ['senior_id' => $level3_id[0]->parent_id,
				           'joinee_id' => $joinee_id,
				           'course' => $course,
				           'incentive_obtained' => ($amt_paid)/8]);
			} catch(QueryException $e) {
				abort( response($e, 403) );
			}

   		} else {
   			abort( response('Course type '.$course.' is not supported', 403) );
   		}
   		return back()->withInput();
   }
}
