<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Hash;

class IncentiveViewController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }

   public function index() {
		$incentives = DB::table('incentives_tbl')
			->join('users', 'users.id', '=', 'incentives_tbl.joinee_id')
            ->select('*')
            ->where('senior_id', '=', Auth::user()->id)
            ->get();
        // print_r($incentives);
      return view('incentive_view',['incentives'=>$incentives]);
   }
}
