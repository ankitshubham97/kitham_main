<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailable extends Mailable
{
    use Queueable, SerializesModels;
    public $name, $password, $course;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $password, $course)
    {
        $this->name = $name;
        $this->password = $password;
        $this->course = $course;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@kitham.in')
                    ->subject('Welcome to Kitham family!')
                    ->view('email.send_password');
    }
}