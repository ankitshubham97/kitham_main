<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/view-downline/{id}','DownlineViewController@index');

Route::get('/add-downline','DownlineAddController@index');

Route::post('/add-downline/handle','DownlineAddHandlerController@index');

Route::get('/view-incentive','IncentiveViewController@index');

Route::get('/process-incentive','IncentiveProcessController@index');

Route::post('/process-incentive/handle','IncentiveProcessHandlerController@index');

Route::get('/pay-incentive','IncentivePayController@index');

Route::post('/pay-incentive/handle','IncentivePayHandlerController@index');

Route::get('/manage-payment','PaymentManageController@index');

Route::post('/manage-payment/handle','PaymentManageHandlerController@index');

Route::get('/view-courses','CoursesViewController@index');

Route::get('/view-courses/html','CoursesViewController@html');
