<?php

return [
    'html' => 'HTML',
    'htmlcss' => 'HTML with CSS',
    'htmlcssjs' => 'HTML with CSS and JavaScript',
];