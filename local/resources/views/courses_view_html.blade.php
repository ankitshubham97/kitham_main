@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                  HTML
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="containter center">
                        <video controls controlsList="nodownload" width="100%" height="auto">
                        <source src="{{URL::asset('videos/html/intro.mp4')}}" type="video/mp4">
                        Your browser does not support the video tag.
                        </video>
                        <div class="overlay">
                            <p>1. Intro</p>
                        </div>
                        <video controls controlsList="nodownload" width="100%" height="auto">
                        <source src="{{URL::asset('videos/html/tags.mp4')}}" type="video/mp4">
                        Your browser does not support the video tag.
                        </video>
                        <div class="overlay">
                            <p>2. HTML tags</p>
                        </div>
                        <video controls controlsList="nodownload" width="100%" height="auto">
                        <source src="{{URL::asset('videos/html/heading.mp4')}}" type="video/mp4">
                        Your browser does not support the video tag.
                        </video>
                        <div class="overlay">
                            <p>3. Heading tags</p>
                        </div>
                        
                    </div>
                </div>
                <div class="card-footer">
                  <a href="/home">{{ __('Home') }}</a>
                </div>  
            </div>
        </div>
    </div>
</div>
@endsection
