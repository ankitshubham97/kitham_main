<div>
    Hi {{ $name }}!
    <br>
    Welcome to Kitham! We are glad to see you onboard. We have recorded that you have opted for 
    {{ $course }}.
    <br>
    <u>It will take upto 24 hours for activating your account</u>. After that, you can login to kitham.in using this email. Your password is '{{ $password }}'. 
    We recommend that you change it for safety purpose.
</div>