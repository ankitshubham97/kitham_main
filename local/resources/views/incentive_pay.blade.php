@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-16">
            <div class="card">
                <div class="card-header">
                  Pay incentive
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="containter center">
                    	<table class="table table-hover table-responsive text-nowrap">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col">Senior ID</th>
								<th scope="col">Senior's UPI</th>
								<th scope="col">Joinee ID</th>
								<th scope="col">Course</th>
								<th scope="col">Incentive Obtained</th>
								<th scope="col">Incentive Transaction ID</th>
							</tr>
						</thead>
						<tbody>
						@foreach ($incentives as $incentive)
							<tr>
								<th scope="row">{{ $loop->iteration }}</th>
								<td>{{ $incentive->senior_id }}</td>
								<td>
									@if (empty($incentive->upi))
										NULL
									@else
										{{ $incentive->upi }}
									@endif
								</td>
								<td>{{ $incentive->joinee_id }}</td>
								<td>{{ $incentive->course }}</td>
								<td>{{ $incentive->incentive_obtained }}</td>
								<td>
									@if (empty($incentive->incentive_transaction_id))
										@if (empty($incentive->upi))
											NULL
										@else
											<form method="post" action="{{action('IncentivePayHandlerController@index')}}">
	                        					@csrf
												<input type="text" name="incentive_transaction_id">
												<input type="hidden" name="id" value="{{ $incentive->id }}">
												<button type="submit" class="btn btn-primary">{{ __('Pay Incentive') }}</button>
	                        				</form>
										@endif
									@else
										@if (empty($incentive->upi))
											<!-- Already handled this case in the calling controller IncentivePayController -->
										@else
											{{ $incentive->incentive_transaction_id }}
										@endif
									@endif
								</td>
							</tr>
						@endforeach
						</tbody>	
                      </table>
                    </div>
                </div>
                <div class="card-footer">
                  <a href="/home">{{ __('Home') }}</a>
                </div>  
            </div>
        </div>
    </div>
</div>
@endsection
