@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                  Process incentive
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="containter center">
                    	<table class="table table-hover table-responsive text-nowrap">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col">Joinee ID</th>
								<th scope="col">Name</th>
								<th scope="col">Email</th>
								<th scope="col">Payment Image Name</th>
								<th scope="col">Amount paid</th>
							</tr>
						</thead>
						
							<tbody>
							<tr> HTML course </tr>
							@foreach ($html_course_payments as $html_course_payment)
								<tr>
									<th scope="row">{{ $loop->iteration }}</th>
									<td>{{ $html_course_payment->joinee_id }}</td>
									<td>{{ $html_course_payment->name }}</td>
									<td>{{ $html_course_payment->email }}</td>
									<td>{{ $html_course_payment->payment_image_name }}</td>
									<td>
										@if (empty($html_course_payment->amt_paid))
											<form method="post" action="{{action('IncentiveProcessHandlerController@index')}}">
	                        					@csrf
												<input type="text" name="amt_paid">
												<input type="hidden" name="joinee_id" value="{{ $html_course_payment->joinee_id }}">
												<input type="hidden" name="course" value="{{ __('messages.html') }}">
												<button type="submit" class="btn btn-primary">{{ __('Update Incentive') }}</button>
	                        				</form>
										@else
											{{ $html_course_payment->amt_paid }}
										@endif
									</td>
								</tr>
							@endforeach
							</tbody>
							
                      </table>
                    </div>
                </div>
                <div class="card-footer">
                  <a href="/home">{{ __('Home') }}</a>
                </div>  
            </div>
        </div>
    </div>
</div>
@endsection
