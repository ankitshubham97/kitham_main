@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Home') }}</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="containter center">
                        <table class="table table-hover table-responsive text-nowrap">
                           <tbody>
                              <tr>
                                 <th scope="row">1</th>
                                 <td><a href=view-downline/{{ $id }}> {{ __('View downline') }} </a></td>
                              </tr>
                              <tr>
                                 <th scope="row">2</th>
                                 <td><a href=add-downline> {{ __('Add downline') }} </a></td>
                              </tr>
                              <tr>
                                 <th scope="row">3</th>
                                 <td><a href=view-incentive> {{ __('View incentive') }} </a></td>
                              </tr>
                              <tr>
                                 <th scope="row">4</th>
                                 <td><a href=manage-payment> {{ __('Manage payment') }} </a></td>
                              </tr>
                              <tr>
                                 <th scope="row">5</th>
                                 <td><a href=view-courses> {{ __('View courses') }} </a></td>
                              </tr>
                           </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
