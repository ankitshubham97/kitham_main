@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Downline Add Failed!') }}</div>

                <div class="card-body">
                    <div class="containter center">
                        Oops {{ Auth::user()->name }}!
                        <br>
                        {{ $errmsg }}
                    </div>
                </div>
                <div class="card-footer">
                  <a href="/home">{{ __('Home') }}</a>
                  <br>
                  <a href="/add-downline">{{ __('Try again') }}</a>
                </div>  
            </div>
        </div>
    </div>
</div>
@endsection
