@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                  Manage Payment
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="containter center">
                    	@if (empty($user[0]->upi))
							<form method="post" action="{{action('PaymentManageHandlerController@index')}}">
            					@csrf
            					<div class="form-group row">
		                            <label for="upi" class="col-md-4 col-form-label text-md-right">{{ __('UPI') }}</label>

		                            <div class="col-md-6">
		                                <input type="text" class="form-control{{ $errors->has('upi') ? ' is-invalid' : '' }}" name="upi" required autofocus>
		                                @if ($errors->has('upi'))
		                                    <span class="invalid-feedback" role="alert">
		                                        <strong>{{ $errors->first('upi') }}</strong>
		                                    </span>
		                                @endif
		                            </div>
		                        </div>
								<input type="hidden" name="id" value="{{ $user[0]->id }}">
								<div class="form-group row mb-0">
		                            <div class="col-md-6 offset-md-4">
		                                <button type="submit" class="btn btn-primary">{{ __('Save UPI') }}</button>
		                            </div>
		                        </div>
								
            				</form>
						@else
							UPI : {{ $user[0]->upi }}
						@endif
                    </div>
                </div>
                <div class="card-footer">
                  <a href="/home">{{ __('Home') }}</a>
                </div>  
            </div>
        </div>
    </div>
</div>
@endsection
