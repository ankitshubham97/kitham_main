@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                  Downline of 
                  <a href="/view-downline/{{ Auth::user()->id }}">{{ Auth::user()->name }}</a>
                  @if (isset($level2))
                     <a href="/view-downline/{{ $level2[0]->id }}">{{ ' : '.$level2[0]->name }}</a>
                  @endif
                  @if (isset($level3))
                     <a href="/view-downline/{{ $level3[0]->id }}">{{ ' : '.$level3[0]->name }}</a>
                  @endif
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="containter center">
                        <table class="table table-hover table-responsive text-nowrap">
                           <thead>
                              <tr>
                                 <th scope="col">#</th>
                                 <th scope="col">Name</th>
                                 <th scope="col">Email</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach ($flat_downlines as $flat_downline)
                              <tr>
                                 <th scope="row">{{ $loop->iteration }}</th>
                                 <td><a href="/view-downline/{{ $flat_downline->child_id }}">{{ $flat_downline->name }}</a></td>
                                 <td>{{ $flat_downline->email }}</td>
                              </tr>
                              @endforeach
                           </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                  <a href="/home">{{ __('Home') }}</a>
                </div>  
            </div>
        </div>
    </div>
</div>
@endsection
