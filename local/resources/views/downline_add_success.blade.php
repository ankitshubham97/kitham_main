@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Downline Add Successful!') }}</div>

                <div class="card-body">
                    <div class="containter center">
                        Congratulations {{ Auth::user()->name }}!
                        <br>
                        {{ $name }} ({{ $email }}) is successfully added to your direct downline.
                    </div>
                </div>
                <div class="card-footer">
                  <a href="/home">{{ __('Home') }}</a>
                </div>  
            </div>
        </div>
    </div>
</div>
@endsection
