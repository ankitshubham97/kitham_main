@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                  View Courses
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="containter center">
                        <table class="table table-hover table-responsive text-nowrap">
                           <thead>
                              <tr>
                                 <th scope="col">Course</th>
                              </tr>
                           </thead>
                           <tbody>
                              @if ($show_html == 1)
                                <tr>
                                  <td><a href="/view-courses/html">{{ __('messages.html') }}</a></td>
                                </tr>
                              @endif
                              @if ($show_html_css == 1)
                                <tr>
                                  <td><a href="/to/htmlcss/course/content">{{ __('messages.htmlcss') }}</a></td>
                                </tr>
                              @endif
                              @if ($show_html_css_js == 1)
                                <tr>
                                  <td><a href="/to/htmlcssjs/course/content">{{ __('messages.htmlcssjs') }}</a></td>
                                </tr>
                              @endif
                           </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                  <a href="/home">{{ __('Home') }}</a>
                </div>  
            </div>
        </div>
    </div>
</div>
@endsection
