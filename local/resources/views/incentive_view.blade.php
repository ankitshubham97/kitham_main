@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                  View incentive
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="containter center">
                        <table class="table table-hover table-responsive text-nowrap">
                           <thead>
                              <tr>
                                 <th scope="col">#</th>
                                 <th scope="col">ID</th>
                                 <th scope="col">Name</th>
                                 <th scope="col">Email</th>
                                 <th scope="col">Course</th>
                                 <th scope="col">Incentive</th>
                                 <th scope="col">Status</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach ($incentives as $incentive)
                              <tr>
                                 <th scope="row">{{ $loop->iteration }}</th>
                                 <td><a href="/view-downline/{{ $incentive->id }}">{{ $incentive->id }}</a></td>
                                 <td><a href="/view-downline/{{ $incentive->id }}">{{ $incentive->name }}</a></td>
                                 <td><a href="/view-downline/{{ $incentive->id }}">{{ $incentive->email }}</a></td>
                                 <td>{{ $incentive->course }}</td>
                                 <td>{{ $incentive->incentive_obtained }}</td>
                                 <td>{{ $incentive->status }}</td>
                              </tr>
                              @endforeach
                           </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                  <strong>Total incentive: {{ $incentives->sum('incentive_obtained') }}</strong>
                  <br>
                  <a href="/home">{{ __('Home') }}</a>
                </div>  
            </div>
        </div>
    </div>
</div>
@endsection
