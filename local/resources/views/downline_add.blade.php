@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Add Downline') }}</div>

                <div class="card-body">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                        </div>
                        <img src="images/{{ Session::get('image') }}">
                        @endif
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                    @endif
                    <form method="post" action="{{action('DownlineAddHandlerController@index')}}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="course" class="col-md-4 col-form-label text-md-right">{{ __('Course') }}</label>

                            <div class="col-md-6">
                                <select name="course">
                                    <option value="{{ __('messages.html') }}" selected>{{ __('messages.html') }} (Fee: Rs 500)</option>
                                    <option value="{{ __('messages.htmlcss') }}">{{ __('messages.htmlcss') }} (Fee: Rs 600)</option>
                                    <option value="{{ __('messages.htmlcssjs') }}">{{ __('messages.htmlcssjs') }} (Fee: Rs 800)</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="image_identity_proof" class="col-md-4 col-form-label text-md-right">Aadhaar Card Photo</label>
                            <div class="col-md-6">
                                <input type="file" name="image_identity_proof" class="form-control{{ $errors->has('image_identity_proof') ? ' is-invalid' : '' }}">
                                @if ($errors->has('image_identity_proof'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('image_identity_proof') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Payment options</label>
                            <div class="col-md-6">
                                UPI id: kitham@icici
                                <br>
                                PayTM: 9971754945
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="image_payment" class="col-md-4 col-form-label text-md-right">{{ __('Payment Proof Screenshot') }}</label>
                            <div class="col-md-6">
                                <input type="file" name="image_payment" class="form-control{{ $errors->has('image_payment') ? ' is-invalid' : '' }}">
                                @if ($errors->has('image_payment'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('image_payment') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Add Downline') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-footer">
                  <a href="/home">{{ __('Home') }}</a>
                </div>  
            </div>
        </div>
    </div>
</div>
@endsection
